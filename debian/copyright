Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: bitmask-vpn
Source: https://0xacab.org/leap/bitmask-vpn/-/tags
Files-Excluded: vendor/github.com/google/uuid
                vendor/github.com/gopherjs/gopherjs
                vendor/github.com/jtolds/gls
                vendor/github.com/kardianos/osext
                vendor/github.com/klauspost/cpuid
                vendor/github.com/klauspost/reedsolomon
                vendor/github.com/mmcloughlin/avo
                vendor/github.com/pkg/errors
                vendor/github.com/ProtonMail/go-autostart
                vendor/github.com/rakyll/statik
                vendor/github.com/sevlyar/go-daemon
                vendor/github.com/smartystreets/assertions
                vendor/github.com/smartystreets/goconvey
                vendor/github.com/tjfoc/gmsm
                vendor/github.com/xtaci/kcp-go
                vendor/github.com/xtaci/smux
                vendor/git.torproject.org/pluggable-transports/goptlib.git
                vendor/golang.org/x/crypto
                vendor/golang.org/x/mod
                vendor/golang.org/x/net
                vendor/golang.org/x/sys
                vendor/golang.org/x/tools
                vendor/golang.org/x/xerrors
                vendor/github.com/cretz/bine
                vendor/github.com/pion/datachannel
                vendor/github.com/pion/dtls
                vendor/github.com/pion/ice
                vendor/github.com/pion/interceptor
                vendor/github.com/pion/logging
                vendor/github.com/pion/mdns
                vendor/github.com/pion/randutil
                vendor/github.com/pion/rtcp
                vendor/github.com/pion/rtp
                vendor/github.com/pion/sctp
                vendor/github.com/pion/sdp
                vendor/github.com/pion/srtp
                vendor/github.com/pion/stun
                vendor/github.com/pion/transport
                vendor/github.com/pion/turn
                vendor/github.com/pion/udp
                vendor/github.com/pion/webrtc

Files: *
Copyright: 2018-2022 LEAP
License: GPL-3

Files: gui/qjsonmodel.*
Copyright: 2011 SCHUTZ Sacha
           2020 Kali Kaneko
License: Expat

Files: gui/components/VPNToggle.qml
       gui/components/VPNButtonBase.qml
Copyright: LEAP
License: MPL-2.0
Comment: No copyr holder mentioned so using LEAP

Files: gui/components/MaterialCheckBox.qml
       gui/components/MaterialRadioIndicator.qml
       gui/components/MaterialRadioButton.qml
Copyright: 2017 Qt Company
License: LGPL-3.0 or GPL-2+

Files: gui/components/FadeBehavior.qml
Copyright: 2020 Pierre-Yves Siret
License: Expat

Files: vendor/github.com/agl/ed25519/*
Copyright: 2012 The Go Authors
License: BSD-3-Clause

Files: vendor/github.com/apparentlymart/go-openvpn-mgmt/*
Copyright: 2016 Martin Atkins
License: Expat
Comment: License as present in README
 https://github.com/apparentlymart/go-openvpn-mgmt/blob/master/README.md

Files: vendor/github.com/dchest/siphash/*
Copyright: Dmitry Chestnykh
License: CC0-1.0

Files: vendor/github.com/keybase/go-ps/*
Copyright: 2014 Mitchell Hashimoto
           2015 Keybase
License: Expat

Files: vendor/github.com/OperatorFoundation/obfs4/*
       vendor/github.com/OperatorFoundation/shapeshifter-transports/*
Copyright: 2014, Yawning Angel <yawning@torproject.org>
           2012 The Go Authors
License: BSD-3-Clause or BSD-2-Clause

Files: vendor/github.com/OperatorFoundation/shapeshifter-ipc/*
Copyright: 2020 Operator Foundation
License: CC0-1.0

Files: vendor/0xacab.org/leap/shapeshifter/*
Copyright: 2019, LEAP <info@leap.se>
           2014, Yawning Angel <yawning@torproject.org>
           2012 The Go Authors
License: BSD-3-Clause or BSD-2-Clause
Comment: Upstream clarified that this is dual licensed - and similar
 copyr is there in obfs4 and shapeshifter-transports as well

Files: vendor/git.torproject.org/pluggable-transports/snowflake.git/*
Copyright: 2016, Serene Han, Arlo Breault
           2019-2020, The Tor Project, Inc
License: BSD-3-Clause

Files: vendor/github.com/templexxx/xorsimd/*
       vendor/github.com/templexxx/cpu/*
Copyright: 2019 Temple3x (temple3x@gmail.com)
License: Expat

Files: vendor/git.torproject.org/pluggable-transports/snowflake.git/common/nat/nat.go
Copyright: 2018 Pion LLC
License: Expat

Files: debian/*
Copyright: 2022 Nilesh Patra <nilesh@debian.org>
License: GPL-3

License: GPL-3
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
 .
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 .
 On Debian systems you can find a copy of the full text of the GNU General
 Public License version 3 at /usr/share/common-licenses/GPL-3.

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 3. Neither the name of the Institute nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

License: BSD-2-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
   Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 .
   Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 OF THE POSSIBILITY OF SUCH DAMAGE.

License: CC0-1.0
 On Debian systems the full text of the CC0-1.0 license can be found in
 /usr/share/common-licenses/CC0-1.0

License: MPL-2.0
 On Debian systems the full text of the MPL-2.0 can be found in
 /usr/share/common-licenses/MPL-2.0.

License: LGPL-3.0
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU Library General Public License as published by the
 Free Software Foundation; version 3 of the License, or (at
 your option) any later version.
 .
 On Debian systems, the complete text of version 3 of the GNU Library
 General Public License can be found in '/usr/share/common-licenses/LGPL-3'.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
